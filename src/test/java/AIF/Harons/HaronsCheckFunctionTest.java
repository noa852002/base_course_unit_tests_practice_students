package AIF.Harons;

//incomplete ...


import static org.hamcrest.CoreMatchers.allOf;
import static org.hamcrest.CoreMatchers.anyOf;
import static org.hamcrest.CoreMatchers.hasItems;
import static org.hamcrest.CoreMatchers.not;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import AIF.AIFUtil;
import AIF.AerialVehicles.AerialVehicle;
import org.junit.BeforeClass;
import org.junit.Test;



public class HaronsCheckFunctionTest {

    public static AIFUtil aifUtil;
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        aifUtil = new AIFUtil();
    }



    @Test//23
    public void testCheckHaronssEquilibriumGroupsInGroup(){
        aifUtil.setHoursAndCheck(aifUtil.getAerialVehiclesHashMap().get("Shoval"),23);
        assertTrue("failure - hoursOfFlightSinceLastRepair = 0 should  0 Shoval.check().", aifUtil.getAerialVehiclesHashMap().get("Shoval").getHoursOfFlightSinceLastRepair() == 23);
    }

    @Test//-6
    public void testCheckHaronssEquilibriumGroupsUnderGroup(){
        aifUtil.setHoursAndCheck(aifUtil.getAerialVehiclesHashMap().get("Shoval"),-6);
        assertTrue("failure - hoursOfFlightSinceLastRepair = -6 should be reset to 0 by Shoval.check().", aifUtil.getAerialVehiclesHashMap().get("Shoval").getHoursOfFlightSinceLastRepair() == 0);
    }

    @Test//299
    public void testCheckHaronssEquilibriumGroupsAboveGroup(){
        aifUtil.setHoursAndCheck(aifUtil.getAerialVehiclesHashMap().get("Shoval"),299);//1444 - arbitrary
        assertTrue("failure - hoursOfFlightSinceLastRepair = 144 shouldn't be reset to 0 after Shoval.check().", aifUtil.getAerialVehiclesHashMap().get("Shoval").getHoursOfFlightSinceLastRepair() == 0);
    }



    //ערכי גבול

    @Test
    public void testCheckLimitsHaronssLowerBoundLeft(){
        aifUtil.setHoursAndCheck(aifUtil.getAerialVehiclesHashMap().get("Shoval"),-1);
        assertTrue("failure - hoursOfFlightSinceLastRepair = -1 should throws an exception ? or reset to zero ?.", aifUtil.getAerialVehiclesHashMap().get("Shoval").getHoursOfFlightSinceLastRepair() == 0);
    }

    @Test
    public void testCheckLimitsHaronssLowerBoundRight(){
        aifUtil.setHoursAndCheck(aifUtil.getAerialVehiclesHashMap().get("Shoval"),1);
        assertTrue("failure - hoursOfFlightSinceLastRepair = 1 should be reset to 1 Shoval.check().", aifUtil.getAerialVehiclesHashMap().get("Shoval").getHoursOfFlightSinceLastRepair() == 1);
    }



    @Test
    public void testCheckLimitsHaronssUpperBoundLeft(){
        aifUtil.setHoursAndCheck(aifUtil.getAerialVehiclesHashMap().get("Shoval"),149);
        assertTrue("failure - hoursOfFlightSinceLastRepair = 150 should be reset to 0 Shoval.check().", aifUtil.getAerialVehiclesHashMap().get("Shoval").getHoursOfFlightSinceLastRepair() == 149);
    }





    @Test
    public void testCheckUniqueEndCasesHarons(){
        aifUtil.setHoursAndCheck(aifUtil.getAerialVehiclesHashMap().get("Shoval"),0);
        assertTrue("failure - hoursOfFlightSinceLastRepair = 0 should stay 0", aifUtil.getAerialVehiclesHashMap().get("Shoval").getHoursOfFlightSinceLastRepair() != 0);

    }


}