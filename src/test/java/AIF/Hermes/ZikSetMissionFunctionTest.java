package AIF.Hermes;

import static org.hamcrest.CoreMatchers.allOf;
import static org.hamcrest.CoreMatchers.anyOf;
import static org.hamcrest.CoreMatchers.hasItems;
import static org.hamcrest.CoreMatchers.not;
import static org.junit.Assert.*;

import AIF.AIFUtil;
import AIF.Missions.*;
import org.junit.BeforeClass;
import org.junit.Test;


public class ZikSetMissionFunctionTest {
    public static AIFUtil aifUtil;
    @BeforeClass
    public static void setUpBeforeClass(){
        aifUtil = new AIFUtil();
    }


    @Test//Executable Mission -> IntelligenceMission
    public void testZikExecutableMission() throws MissionTypeException{
        try{
            aifUtil.getAerialVehiclesHashMap().get("Zik").setMission(aifUtil.getAllMissions().get("intelligence"));
            assertTrue(true);
        }catch (MissionTypeException missionTypeException){//if exception is thrown it is mean that mission cannot be deploy with the curen aerial vehicle.
            fail();

        }
    }

    @Test//Executable Mission -> AttackMission
    public void testZikUnexecutableMission() throws MissionTypeException{
        try{
            aifUtil.getAerialVehiclesHashMap().get("Zik").setMission(aifUtil.getAllMissions().get("Attack"));
            fail();
        }catch (MissionTypeException missionTypeException){
            assertTrue(true);
        }
    }
}
